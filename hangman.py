#!/usr/bin/env python
# the shebang may or not be necessary, but it can solve problems.
# essentially it ensures that when running this program in shell it will use your current (active) python environment.
# you can also specify for it to use python3 on some systems by entering "/usr/bin/python3" instead

# Made by Vincent Chernin

# This game has been tested to work python 3.7, however it should work fine on any version of python 3.

import random
import os
import sys

# IMPORTANT
# Ensure that you download the "words" file along with the program, and that it is in the same directory as the python
# file. The program uses this file to find words, and if it is not present nothing will work.

# The code to restart the program (used os and sys) at the end may not work in windows directories with spaces in them.

# nicely put the word file into a list.
all_words = open('words').read().splitlines()

# make sure it is lower case before obtaining which removes potential complications later on.
# we will always convert to lower case in this game
reasonable_secret_word = False
while reasonable_secret_word is False:
    secret_word_str = str.lower(random.choice(all_words))
    # find this once so we don't need to recalculate it
    secret_word_length = len(secret_word_str)
    # stop trying to find a word if it is easy enough to guess
    if secret_word_length < 10:
        reasonable_secret_word = True
    secret_word_list = list(secret_word_str)

# now we have our word that is not too difficult to guess we can begin the program

# this will be used so we can end the game when necessary
run_game = True


def get_type_of_guess():
    """asks the user whether they would like to guess the word or a letter"""
    while True:
        choice_desired_type_of_guess = str.lower(input('Would you like to try and guess the word now? (Y/n) '))
        if choice_desired_type_of_guess == 'y' or choice_desired_type_of_guess == 'ye' \
                or choice_desired_type_of_guess == 'yes':
            print('')
            return 'type_guess_word'
        else:
            print('')
            return 'type_guess_letter'


def get_word_guess():
    """this is for when the user wants to guess the word itself"""
    ask_desired_word_guess = True

    while ask_desired_word_guess is True:
        reasonable_guess = True
        word_guess = str.lower(input('What would you like to guess as the word? '))

        if str(word_guess) == str(secret_word_str):
            print('The game is over, you have found the word! ')
            return False  # this is for run_game, so we are stopping the game

        elif len(word_guess) != secret_word_length:
            print('The length of your guess should be the same as the amount '
                  'of possible letters, which is ', secret_word_length, '.', sep='')
            print('Please enter a valid possibility for the word.')
            print('')
            reasonable_guess = False

        else:
            print('Sorry, but you have guessed incorrectly.')
            print('')

        # see if user wants to guess word a second time or more
        # we need a new variable to ensure we give the user what they want
        desired_word_guess_retry_is_valid = False
        while reasonable_guess is True and desired_word_guess_retry_is_valid is False:
            desired_word_guess_retry = str.lower(input('Would you like to try and guess the word again? (Y/n) '))
            print('')

            if desired_word_guess_retry == 'y' or desired_word_guess_retry == 'ye' \
                    or desired_word_guess_retry == 'yes':
                desired_word_guess_retry_is_valid = True
                print('Here\'s another chance to try again!')
                print('')

            else:
                should_ask_type_guess = False
                return True, should_ask_type_guess  # this is for run_game, so we are keeping the
                # game running as the user did not guess right


def get_letter_guess(user_letter_guesses):
    """this gets a user input for a letter guess each time it is necessary
    output a specific error for what the user has done wrong.
    Also adds to the count of which letters the user has guessed, so they cannot guess the same letter twice."""
    while True:
        desired_letter_guess_is_valid = True
        desired_letter_guess = str.lower(input('What letter would you like to try? '))

        # we need to catch errors otherwise a correct input will cause this is_integer() method to crash the program.
        try:
            if len(desired_letter_guess) == 1 and float(desired_letter_guess).is_integer() is True:
                desired_letter_guess_is_valid = False
                print('You cannot guess a number as a letter.')
                print('')

        except ValueError:
            if len(desired_letter_guess) == 1:
                # make sure the user has not guessed this letter before
                if user_letter_guesses.count(desired_letter_guess) == 0:
                    user_letter_guesses.append(desired_letter_guess)
                    return desired_letter_guess, user_letter_guesses
                else:
                    print('Please enter a letter which you have not yet guessed.')
                    print('')
                    desired_letter_guess_is_valid = False
        if desired_letter_guess_is_valid is True:
            print('Please enter just one letter as your guess.')
            print('')


def find_all_guess_letter_occurrences(secret_word_str, current_letter_guess):
    """find the indexes in the secret word at which the letter guess occur at."""
    start = 0
    while True:
        start = secret_word_str.find(current_letter_guess, start)
        if start == -1:
            return
        yield start
        start += len(current_letter_guess)  # use start += 1 to find overlapping matches


# use this to remake the pattern
def create_guessed_word_progress(current_letter_guess, current_guess_letter_occurrences, current_word_with_guesses):
    """make a object that represents how much the user has guessed, for apple it may look like 'a _ _ l _'"""
    for i in range(0, len(guess_letter_occurrences)):
        # we need to multiply it by 2 as we found the index of the guess letter occurrences as a string
        current_word_with_guesses[current_guess_letter_occurrences[i] * 2] = current_letter_guess
    return current_word_with_guesses


# beginning statements only used once
print('Welcome to hangman!')

print('A word has been chosen, the outline looks like this: ')
word_with_guesses_str = '_ ' * secret_word_length
print('')
# we will use this later.
total_word_progress = list(word_with_guesses_str)

print(word_with_guesses_str)
print('')

total_user_letter_guesses = []
ask_type_guess = True  # we need this for a specific case, where the program asks whether the user wants to reguess.
# the word, and if they say no we should not ask them for the type of guess.

# do this try & except to make KeyboardInterrupt a bit smoother.
try:
    while run_game is True:

        # run the get type of guess
        if ask_type_guess is True:
            type_guess = get_type_of_guess()
        # will either be type_guess_word or type_guess_letter

        # run this is someone chooses to guess the word
        if type_guess is 'type_guess_word' and ask_type_guess is True:
            get_word_guess_output = get_word_guess()

            run_game = get_word_guess_output[0]
            ask_type_guess = get_word_guess_output[1]
        else:
            # get the letter guess from the user, while ensuring it as not been guessed before.
            get_letter_guess_output = get_letter_guess(total_user_letter_guesses)
            letter_guess = get_letter_guess_output[0]
            total_user_letter_guesses = get_letter_guess_output[1]

            # see if the letter guess is actually in the secret word.

            # see if the letter guess is actually in the secret word.
            guess_letter_occurrences = list(find_all_guess_letter_occurrences(secret_word_str, letter_guess))
            if guess_letter_occurrences != []:
                print('That letter is indeed present in the secret word! ')
                print('')
            else:
                print('That letter is not present in the secret word. ')
                print('')

            ask_type_guess = True  # reset this to ensure the type is asked again

            # now that we have a new guess, we can print a new version of the word.
            total_word_progress = create_guessed_word_progress(letter_guess, guess_letter_occurrences,
                                                               total_word_progress)
            # show the player how much progress they made
            print(''.join(total_word_progress))

            if '_' in total_word_progress:
                print('')
            else:
                run_game = False
    print('')
    print('Good job, you have figured out the word!')
    print('You have determined this in ', len(total_user_letter_guesses), ' guesses.', sep='')  # show guess count
except KeyboardInterrupt:
    print('')
    print('')
    print('You have ended the game early, the word was ', secret_word_str, '.', sep='')

print('')
desire_restart_game = input('Would you like to play again? (Y/n) ')
if desire_restart_game == 'yes' or desire_restart_game == 'ye' or desire_restart_game == 'y':
    print('')
    os.execl(sys.executable, sys.executable, *sys.argv)  # restarts the program,
    # may not work in windows directories with spaces
